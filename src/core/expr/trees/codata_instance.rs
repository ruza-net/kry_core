use std::collections::HashMap;

use crate::utils::err_msg;
use crate::utils::ty_tools;
use crate::utils::errors::CopatternError;

use crate::core::types;
use crate::core::traits::*;
use crate::core::expr::Expr;
use crate::core::context::Context;


type TreePtr<'a> = *mut types::trees::codata::Codata<'a>;

#[derive(Debug, Clone)]
pub struct CodataInstance<'a> {
    ty: TreePtr<'a>,

    eliminators: HashMap<&'a str, Expr<'a>>,
}

// NOTE: `specify` and `call_eliminator` have unsafe blocks.
//
impl<'a> CodataInstance<'a> {
    pub fn new(ty: TreePtr<'a>) -> Self {
        CodataInstance {
            ty,

            eliminators: HashMap::new(),
        }
    }

    pub fn specify(&mut self, name: &'a str, val: Expr<'a>) {
        assert![!self.eliminators.contains_key(name), err_msg::d_tree_def_conflict(name)];

        unsafe {
            match (*self.ty).get_item(name) {
                None => panic![err_msg::d_tree_item_not_decl(name)],

                Some(f) => {
                    let (elim, ty) = f.get_target();

                    let mut new_ty = ty.clone();

                    if let Some(name) = elim {
                        new_ty.substitute_val(name.to_string(), Expr::CodataInstance(self as *mut _));
                    }

                    assert_eq![new_ty, val.get_type(), "{}", err_msg::func_ret_ty_conflict(name)];

                    self.eliminators.insert(name, val);
                },
            }
        }
    }

    pub fn call_eliminator(&self, name: &'a str) -> Result<&Expr<'a>, CopatternError<'a>> {
        match self.eliminators.get(name) {
            None => Err(
                unsafe {
                    if (*self.ty).get_item(name).is_none() {
                        CopatternError::NoSuchEliminator(name)

                    } else {
                        CopatternError::EliminatorNotSpecified(name)
                    }
                }
            ),

            Some(val) => Ok(val),
        }
    }
}

impl<'a> Substitutable<'a> for CodataInstance<'a> {
    fn rename(&mut self, orig: &str, new: String) {
        for e in self.eliminators.values_mut() {
            e.rename(orig, new.clone());
        }

        unsafe {
            (*self.ty).rename(orig, new);
        }
    }

    fn substitute_val(&mut self, name: String, val: Expr<'a>) {
        ty_tools::check_subst(&self.get_context(), &name, &val);

        for e in self.eliminators.values_mut() {
            e.substitute_val(name.clone(), val.clone());
        }

        unsafe {
            (*self.ty).substitute_val(name, val);
        }
    }
}

impl<'a> Contextual<'a> for CodataInstance<'a> {
    fn get_context(&self) -> &Context<'a> {
        unsafe {
            (*self.ty).get_context()
        }
    }

    fn get_context_mut(&mut self) -> &mut Context<'a> {
        unsafe {
            (*self.ty).get_context_mut()
        }
    }
}

impl<'a> Typeable<'a> for CodataInstance<'a> {
    fn get_type(&self) -> types::Type<'a> {
        types::Type::Codata(self.ty)
    }
}

impl<'a> Reducible<'a> for CodataInstance<'a> {
    fn reduce(&mut self, under: &Context<'a>) {
        assert![self.get_context().generalizes(under), err_msg::REDUCE_CTX_CONFLICT];

        unsafe {
            (*self.ty).reduce(under);
        }

        for v in self.eliminators.values_mut() {
            (*v).reduce(under);
        }
    }
}

impl<'a> PartialEq for CodataInstance<'a> {
    fn eq(&self, rhs: &Self) -> bool {
        self.eliminators == rhs.eliminators &&
        std::ptr::eq(self.ty, rhs.ty)
    }
}
