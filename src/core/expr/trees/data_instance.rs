use std::fmt::Debug;
use std::hash::Hash;
use std::cmp::Ordering;
use std::collections::{ BTreeMap, HashSet };

use crate::utils::err_msg;
use crate::utils::ty_tools;
use crate::utils::mut_tools::Lock;
use crate::utils::errors::PatternError;

use crate::core::traits::*;
use crate::core::expr::Expr;
use crate::core::types::Type;
use crate::core::context::Context;
use crate::core::types::trees::data::Data;


// AREA: Data Instance
//
#[derive(Debug, Clone)]
pub struct DataInstance<'a> {
    ty: *mut Data<'a>,
    mut_lock: Lock,

    cons_name: &'a str,
    cons_arg: Expr<'a>,
}

impl<'a> DataInstance<'a> {
    pub fn new(ty: *mut Data<'a>, cons_name: &'a str, cons_arg: Expr<'a>) -> DataInstance<'a> {
        unsafe {
            if let Some(f) = (*ty).get_item(cons_name) {
                assert_eq![*f.get_source(), cons_arg.get_type(), "{}", err_msg::func_arg_ty_conflict(cons_name)];

            } else {
                panic![err_msg::c_tree_item_not_decl(cons_name)]
            }
        }

        DataInstance {
            ty,
            mut_lock: Lock::off(),

            cons_name,
            cons_arg,
        }
    }

    pub fn get_cons(&self) -> (&'a str, &Expr<'a>) {
        (self.cons_name, &self.cons_arg)
    }
}

impl<'a> Substitutable<'a> for DataInstance<'a> {
    fn rename(&mut self, orig: &str, new: String) {
        if orig != new {
            if !self.mut_lock.rename {
                self.mut_lock.rename = true;

                unsafe { (*self.ty).rename(orig, new.clone()) }

                self.cons_arg.rename(orig, new);

                self.mut_lock.rename = false;
            }
        }
    }

    fn substitute_val(&mut self, name: String, val: Expr<'a>) {
        if !self.mut_lock.substitute_val {
            unsafe { (*self.ty).substitute_val(name.clone(), val.clone()) }

            self.cons_arg.substitute_val(name, val);
        }
    }
}

impl<'a> Reducible<'a> for DataInstance<'a> {
    fn reduce(&mut self, under: &Context<'a>) {
        assert![self.get_context().generalizes(under), err_msg::REDUCE_CTX_CONFLICT];

        unsafe { (*self.ty).reduce(under) }

        self.cons_arg.reduce(under);
    }
}

impl<'a> Contextual<'a> for DataInstance<'a> {
    fn get_context(&self) -> &Context<'a> {
        self.cons_arg.get_context()
    }

    fn get_context_mut(&mut self) -> &mut Context<'a> {
        self.cons_arg.get_context_mut()
    }
}

impl<'a> Typeable<'a> for DataInstance<'a> {
    fn get_type(&self) -> Type<'a> {
        Type::Data(self.ty)
    }
}

impl<'a> PartialEq for DataInstance<'a> {
    fn eq(&self, other: &Self) -> bool {
        std::ptr::eq(self.ty, other.ty) &&
        self.cons_name == other.cons_name &&
        self.cons_arg == other.cons_arg
    }
}

impl<'a> PartialOrd for DataInstance<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        if std::ptr::eq(self.ty, other.ty) && self.cons_name == other.cons_name {
            self.cons_arg.partial_cmp(&other.cons_arg)

        } else {
            None
        }
    }
}
//
// END: Data Instance

// AREA: Pattern Matching
//
#[derive(Debug, Clone, PartialEq)]
pub struct PatternMatching<'a> {
    input: Box<Expr<'a>>,
    ty: Type<'a>,

    pats: BTreeMap<Pattern<'a, &'a str>, Expr<'a>>,
}

impl<'a> PatternMatching<'a> {
    pub fn new(input: Expr<'a>, ty: Type<'a>) -> Self {
        if let Type::Data(_) = input.get_type() {} else { panic![err_msg::MATCH_NOT_ON_DATA] }

        PatternMatching {
            input: Box::new(input),
            ty,

            pats: BTreeMap::new(),
        }
    }

    pub fn add_pat(&mut self, pat: Pattern<'a, &'a str>, expr: Expr<'a>) {
        assert_eq![self.ty, expr.get_type(), "{}", err_msg::pat_type_inconsistent(&pat)];
        assert![!self.pats.contains_key(&pat), err_msg::pat_exists(&pat)];

        assert![!self.ty.get_context().binds_name(pat.get_captures()), err_msg::pat_captures_conflict(&pat)];

        self.pats.insert(pat, expr);
    }

    pub unsafe fn is_complete(&self) -> bool {
        if let Type::Data(data_p) = self.input.get_type() {
            let mut to_refute: HashSet<_> = (*data_p).get_items().keys().collect();

            for pat in self.pats.keys() {
                match pat {
                    Pattern::Capture(_) => to_refute.clear(),

                    Pattern::Constructor(name, _) => assert![to_refute.remove(name), err_msg::match_invalid_cons(name)],
                }
            }

            to_refute.is_empty()

        } else {
            unreachable![]
        }
    }
}

impl<'a> Substitutable<'a> for PatternMatching<'a> {
    fn rename(&mut self, orig: &str, new: String)  {
        for pat_val in self.pats.values_mut() {
            (*pat_val).rename(orig, new.clone());
        }

        self.input.rename(orig, new.clone());
        self.ty.rename(orig, new);
    }

    fn substitute_val(&mut self, name: String, val: Expr<'a>) {
        ty_tools::check_subst(&self.get_context(), &name, &val);

        for pat_val in self.pats.values_mut() {
            (*pat_val).substitute_val(name.clone(), val.clone());
        }

        self.input.substitute_val(name.clone(), val.clone());
        self.ty.substitute_val(name, val);
    }
}

impl<'a> Contextual<'a> for PatternMatching<'a> {
    fn get_context(&self) -> &Context<'a> {
        self.input.get_context()
    }

    fn get_context_mut(&mut self) -> &mut Context<'a> {
        self.input.get_context_mut()
    }
}

impl<'a> Typeable<'a> for PatternMatching<'a> {
    fn get_type(&self) -> Type<'a> {
        self.ty.clone()
    }
}

impl<'a> ReducibleInto<'a, Expr<'a>> for PatternMatching<'a> {
    fn reduce(self, under: &Context<'a>) -> Expr<'a> {
        unsafe { assert![self.is_complete(), err_msg::PATS_NOT_REFUTABLE] }

        let mut result = *self.input;
        result.reduce(under);

        for (pat, ret) in self.pats {
            let destructured = pat.destruct(result.clone());

            if let Ok((capture, val)) = destructured {
                result = ret;

                if result.get_context().binds_name(capture) {
                    result.substitute_val(capture.to_string(), val);
                }

                break;
            }
        }

        result
    }
}

impl<'a> PartialOrd for PatternMatching<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        let mut ord = ty_tools::combine_partial_ord(
            self.input.partial_cmp(&other.input),
            self.ty.partial_cmp(&other.ty),
        );

        // TODO: Compare the pattern maps.

        ord
    }
}
//
// END: Pattern Matching


// The type parameter is to enable using `Pattern` as a representation of `DataInstance`
//
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Pattern<'a, E> {
    Constructor(&'a str, Box<Self>),

    Capture(E),
}

impl<'a, E> Pattern<'a, E> {
    pub fn new_capture(s: E) -> Self {
        Pattern::Capture(s)
    }

    pub fn new_constructor(name: &'a str, inner_pat: Pattern<'a, E>) -> Self {
        Pattern::Constructor(name, Box::new(inner_pat))
    }

    pub fn get_captures(&self) -> &E {
        match self {
            Pattern::Capture(s) => s,

            Pattern::Constructor( _, inner_pat) => inner_pat.get_captures(),
        }
    }
}

// NOTE: The trait constraints cannot be split.
//
impl<'a, E: Eq + Hash + Clone + Debug> Pattern<'a, E> {
    pub fn destruct(self, expr: Expr<'a>) -> Result<(E, Expr<'a>), PatternError<'a, E>> {
        match self {
            Pattern::Capture(e) => Ok((e, expr)),

            Pattern::Constructor(name, inner_pat) => {
                match expr {
                    Expr::DataInstance(inst_p) => unsafe {
                        if (*inst_p).cons_name == name {
                            inner_pat.destruct((*inst_p).cons_arg.clone())

                        } else {
                            Err(
                                PatternError::NotMatching(
                                    Pattern::Constructor(name, inner_pat),
                                    Expr::DataInstance(inst_p),
                                )
                            )
                        }
                    },

                    expr => Err(
                        PatternError::NotMatching(
                            Pattern::Constructor(name, inner_pat),
                            expr,
                        )
                    ),
                }
            },
        }
    }
}


// Ordering such that `Capture` comes last. It is handy when matching a value against a pattern,
// in which situation the capture comes after the specific constructors. That is intended behaviour.
// In case things get equal, orthographic ordering on constructor/capture names is used.
//
impl<'a, E: Ord> Ord for Pattern<'a, E> {
    fn cmp(&self, other: &Self) -> Ordering {
        use Pattern::*;

        match (self, other) {
            (Capture(a), Capture(b)) => a.cmp(b),

            (Capture(_), _) => Ordering::Greater,
            (_, Capture(_)) => Ordering::Less,

            (Constructor(name_a, pat_a), Constructor(name_b, pat_b)) => {
                let pat_ord = pat_a.cmp(pat_b);

                if let Ordering::Equal = pat_ord {
                    name_a.cmp(name_b)

                } else {
                    pat_ord
                }
            },
        }
    }
}
impl<'a, E: Ord> PartialOrd for Pattern<'a, E> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
