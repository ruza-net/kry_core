use crate::utils::err_msg;
use crate::utils::ty_tools;
use crate::utils::declaration::Decl;

use crate::core::types;
use crate::core::traits::*;
use crate::core::expr::Expr;
use crate::core::context::Context;

type FuncT<'a> = types::function_type::FunctionT<'a>;

#[derive(Clone, Debug, PartialEq)]
pub struct Function<'a> {
    param_name: String,
    param_ty: types::Type<'a>,

    body: Box<Expr<'a>>,
    ty: FuncT<'a>,
}

impl<'a> Function<'a> {
    pub fn new(param_name: String, param_ty: types::Type<'a>, body: Expr<'a>, ty: FuncT<'a>) -> Self {
        let mut inner_context = param_ty.get_context().clone();
        inner_context.rebind_val(param_name.clone(), Decl::Declared(param_ty.clone()));

        assert![body.get_context().generalizes(&inner_context), err_msg::func_ctx_conflict(&param_name)];

        assert![*ty.get_source() <= param_ty, "{}", err_msg::func_arg_ty_conflict("")];

        {
            let (elim, target) = ty.get_target();

            if let Some(name) = elim {
                assert_eq![param_name, *name, "{}", err_msg::func_arg_name_conflict(&param_name, name)];

            }

            assert![*target <= body.get_type(), err_msg::func_ret_ty_conflict("")];
        }

        Function {
            param_name,
            param_ty,

            body: Box::new(body),
            ty,
        }
    }

    pub fn call(self, argument: Expr<'a>) -> Expr<'a> {
        let mut arg_context = argument.get_context().clone();
        assert![self.get_context().generalizes(&arg_context), err_msg::REDUCE_CTX_CONFLICT];

        let mut body = *self.body;

        body.substitute_val(self.param_name.clone(), argument.clone());
        arg_context.rebind_val(self.param_name, Decl::Defined(argument));

        body.reduce(&arg_context);

        body
    }
}

impl<'a> Substitutable<'a> for Function<'a> {
    fn rename(&mut self, orig: &str, new: String) {
        if orig != new {
            let param_name = self.get_context().generate_fresh(&self.param_name, self.param_name == new);

            self.body.rename(&self.param_name, param_name.clone());


            self.param_ty.rename(orig, new.clone());
            self.body.rename(orig, new);
        }
    }

    fn substitute_val(&mut self, name: String, val: Expr<'a>) {
        ty_tools::check_subst(&self.get_context(), &name, &val);

        // In case `val` contains the name `self.param_name`
        //
        let new_param_name = val.get_context().generate_fresh(&self.param_name, false);

        self.body.rename(&self.param_name, new_param_name.clone());

        self.param_name = new_param_name;

        self.body.substitute_val(name.clone(), val.clone());
        self.param_ty.substitute_val(name, val);
    }
}

impl<'a> Contextual<'a> for Function<'a> {
    fn get_context(&self) -> &Context<'a> {
        self.param_ty.get_context()
    }

    fn get_context_mut(&mut self) -> &mut Context<'a> {
        self.param_ty.get_context_mut()
    }
}

impl<'a> Typeable<'a> for Function<'a> {
    fn get_type(&self) -> types::Type<'a> {
        types::Type::Function(self.ty.clone())
    }
}

impl<'a> ReducibleInto<'a, Expr<'a>> for Function<'a> {
    fn reduce(self, under: &Context<'a>) -> Expr<'a> {
        assert![self.get_context().generalizes(under), err_msg::REDUCE_CTX_CONFLICT];

        // In case `under` contains the name `self.param_name`
        //
        let param_name = under.generate_fresh(&self.param_name, false);

        // No need to `rename`, since `self.param_name` cannot be bound in `self.get_context()`
        //
        let mut param_ty = self.param_ty;

        param_ty.reduce(under);

        let mut new_under = under.clone();
        new_under.rebind_val(param_name.clone(), Decl::Declared(param_ty.clone()));

        let mut body = self.body;

        body.rename(&self.param_name, param_name.clone());
        body.reduce(&new_under);

        // Check for eta-conversion
        //
        if let Expr::Call(c) = *body {
            if let Expr::Atom(s, _) = c.get_argument() {
                if *s == self.param_name {
                    c.get_function().clone()//FIXME

                } else {
                    Expr::Function(Function::new(param_name, param_ty, Expr::Call(c), self.ty))
                }

            } else {
                Expr::Function(Function::new(param_name, param_ty, Expr::Call(c), self.ty))
            }

        } else {
            Expr::Function(Function::new(param_name, param_ty, *body, self.ty))
        }
    }
}

impl<'a> PartialOrd for Function<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        if self.param_name == other.param_name {
            let p_ty_ord = self.param_ty.partial_cmp(&other.param_ty);
            let b_ord = self.body.partial_cmp(&other.body);
            let ty_ord = self.ty.partial_cmp(&other.ty);

            ty_tools::combine_partial_ord(
                ty_tools::combine_partial_ord(p_ty_ord, b_ord),
                ty_ord,
            )

        } else {
            None
        }
    }
}
