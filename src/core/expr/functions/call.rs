use crate::utils::err_msg;
use crate::utils::ty_tools;

use crate::core::traits::*;
use crate::core::expr::Expr;
use crate::core::types::Type;
use crate::core::context::Context;


#[derive(Debug, Clone, PartialEq)]
pub struct Call<'a> {
    function: Box<Expr<'a>>,
    argument: Box<Expr<'a>>,

    final_type: Type<'a>,
}

impl<'a> Call<'a> {
    pub fn new(function: Expr<'a>, argument: Expr<'a>) -> Self {
        //
        // FIXME: Should it check `function.get_context()` and `argument.get_context()` compatibility?

        let arg_ty = &argument.get_type();

        let final_type = match function.get_type() {
            Type::Function(f) => {
                assert_eq![f.get_source(), arg_ty, "{}", err_msg::subst_type_conflict("function parameter")];

                let (elim, ty) = f.get_target();

                let mut ty = ty.clone();

                if let Some(name) = elim {
                    ty.substitute_val(name.to_string(), argument.clone());
                }

                ty
            },

            _ => panic![err_msg::CALL_TY_CONFLICT],
        };

        Call {
            final_type,

            function: Box::new(function),
            argument: Box::new(argument),
        }
    }

    pub fn get_argument(&self) -> &Expr<'a> {
        &self.argument
    }

    pub fn get_function(&self) -> &Expr<'a> {
        &self.function
    }
}

impl<'a> Substitutable<'a> for Call<'a> {
    fn rename(&mut self, orig: &str, new: String) {
        self.argument.rename(orig.clone(), new.clone());

        self.function.rename(orig, new);
    }

    fn substitute_val(&mut self, name: String, val: Expr<'a>) {
        //
        // FIXME: Possible name conflicts (unchecked contexts).

        ty_tools::check_subst(&self.get_context(), &name, &val);

        self.final_type.substitute_val(name.clone(), val.clone());

        self.argument.substitute_val(name.clone(), val.clone());

        self.function.substitute_val(name, val);
    }
}

impl<'a> Contextual<'a> for Call<'a> {
    fn get_context(&self) -> &Context<'a> {
        self.final_type.get_context()
    }

    fn get_context_mut(&mut self) -> &mut Context<'a> {
        self.final_type.get_context_mut()
    }
}

impl<'a> Typeable<'a> for Call<'a> {
    fn get_type(&self) -> Type<'a> {
        self.final_type.clone()
    }
}

impl<'a> ReducibleInto<'a, Expr<'a>> for Call<'a> {
    fn reduce(self, under: &Context<'a>) -> Expr<'a> {
        assert![self.get_context().generalizes(under), err_msg::REDUCE_CTX_CONFLICT];

        // NOTE: Needed implicit `reduce`
        //
        let mut function = self.function;

        function.reduce(under);

        // Eta-conversion
        //
        // NOTE: No unneeded implicit `reduce`
        //
        match *function {
            // NOTE: No unneeded implicit `reduce`
            //
            Expr::Function(p) => p.call(*self.argument),

            f => Expr::Call(Call::new(f, *self.argument)),
            //
            // FIXME: Maybe add cases that are always invalid
        }
    }
}

impl<'a> PartialOrd for Call<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        let f_ord = self.function.partial_cmp(&other.function);
        let a_ord = self.argument.partial_cmp(&other.argument);
        let ty_ord = self.final_type.partial_cmp(&other.final_type);

        ty_tools::combine_partial_ord(
            ty_tools::combine_partial_ord(f_ord, a_ord),
            ty_ord,
        )
    }
}
