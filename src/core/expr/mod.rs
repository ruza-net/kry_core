pub mod trees;
pub mod functions;

use crate::utils::err_msg;
use crate::utils::ty_tools;
use crate::utils::declaration::Decl;

use crate::core::types;
use crate::core::traits::*;
use crate::core::context::Context;

#[derive(Clone, Debug, PartialEq)]
pub enum Expr<'a> {
    Atom(String, Context<'a>),
    TypeExpr(types::Type<'a>),

    Call(functions::call::Call<'a>),
    Function(functions::function::Function<'a>),

    DataInstance(*mut trees::data_instance::DataInstance<'a>),
    CodataInstance(*mut trees::codata_instance::CodataInstance<'a>),

    PatternMatching(trees::data_instance::PatternMatching<'a>),
    // CodataDispatch(*mut trees::divergent_tree::CodataDispatch<'a>),
}

impl<'a> Substitutable<'a> for Expr<'a> {
    fn rename(&mut self, orig: &str, new: String) {
        if orig != new {
            use Expr::*;

            match self {
                Atom(ref mut s, ref mut c)  => {
                    if s == orig {
                        *s = new.clone();
                    }

                    (*c).rename(orig, new);
                },

                TypeExpr(ty)                => (*ty).rename(orig, new),

                Call(c)                     => c.rename(orig, new),

                Function(f)                 => f.rename(orig, new),

                DataInstance(p)             => unsafe { (**p).rename(orig, new) },
                CodataInstance(p)           => unsafe { (**p).rename(orig, new) },

                PatternMatching(m)          => m.rename(orig, new),
            }
        }
    }

    fn substitute_val(&mut self, name: String, val: Self) {
        ty_tools::check_subst(&self.get_context(), &name, &val);

        use Expr::*;

        match self {
            Atom(s, ref mut c)  => {
                if *s == name {
                    *self = val;

                } else {
                    c.rebind_val(name, Decl::Defined(val));
                }
            },

            TypeExpr(ty)        => (*ty).substitute_val(name, val),

            Call(c)             => c.substitute_val(name, val),

            Function(f)         => f.substitute_val(name, val),

            DataInstance(t)     => unsafe { (**t).substitute_val(name, val) },
            CodataInstance(t)   => unsafe { (**t).substitute_val(name, val) },

            PatternMatching(m)  => m.substitute_val(name, val),
        }
    }
}

impl<'a> Contextual<'a> for Expr<'a> {
    fn get_context(&self) -> &Context<'a> {
        use Expr::*;
        match self {
            Atom(_, c)          => &c,

            TypeExpr(ty)        => ty.get_context(),

            Call(p)             => p.get_context(),

            Function(p)         => p.get_context(),

            DataInstance(p)     => unsafe { (**p).get_context() },
            CodataInstance(p)   => unsafe { (**p).get_context() },

            PatternMatching(m)  => m.get_context(),
        }
    }

    fn get_context_mut(&mut self) -> &mut Context<'a> {
        use Expr::*;
        match self {
            Atom(_, ref mut c)          => c,

            TypeExpr(ty)        => ty.get_context_mut(),

            Call(p)             => p.get_context_mut(),

            Function(p)         => p.get_context_mut(),

            DataInstance(p)     => unsafe { (**p).get_context_mut() },
            CodataInstance(p)   => unsafe { (**p).get_context_mut() },

            PatternMatching(m)  => m.get_context_mut(),
        }
    }
}

impl<'a> Typeable<'a> for Expr<'a> {
    fn get_type(&self) -> types::Type<'a> {
        use Expr::*;
        match self {
            Atom(s, c)          => {
                match c.get_val(s) {
                    Decl::Nothing => panic![err_msg::ctx_name_not_declared(s)],

                    Decl::Declared(ty) => ty.clone(),

                    Decl::Defined(exp) => exp.get_type(),
                }
            },

            TypeExpr(ty)        => ty.get_type(),

            Call(p)             => p.get_type(),

            Function(p)         => p.get_type(),

            DataInstance(p)     => unsafe { (**p).get_type() },
            CodataInstance(p)   => unsafe { (**p).get_type() },

            PatternMatching(m)  => m.get_type(),
        }
    }
}

impl<'a> Reducible<'a> for Expr<'a> {
    fn reduce(&mut self, under: &Context<'a>) {
        assert![self.get_context().generalizes(under), err_msg::REDUCE_CTX_CONFLICT];

        use Expr::*;
        match self {
            Atom(name, _)               => {
                if let Decl::Defined(exp) = under.get_val(name) {
                    *self = exp;

                } else {
                    *self = Atom(name.to_owned(), under.clone());
                }
            },

            TypeExpr(ty)                => ty.reduce(under),

            Call(c)                     => *self = c.clone().reduce(under),

            Function(f)                 => *self = f.clone().reduce(under),

            DataInstance(data_p)        => unsafe { (**data_p).reduce(under) },
            CodataInstance(codata_p)    => unsafe { (**codata_p).reduce(under) },

            PatternMatching(pm)         => *self = pm.clone().reduce(under),
        }
    }
}

impl<'a> PartialOrd for Expr<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        use Expr::*;
        match (self, other) {
            (Atom(name_1, ctx_1), Atom(name_2, ctx_2))      => {
                if name_1 == name_2 {
                    ctx_1.partial_cmp(ctx_2)

                } else {
                    None
                }
            },

            (TypeExpr(ty_1), TypeExpr(ty_2))                => ty_1.partial_cmp(ty_2),

            (Call(c_1), Call(c_2))                          => c_1.partial_cmp(c_2),

            (Function(f_1), Function(f_2))                  => f_1.partial_cmp(f_2),

            (DataInstance(i_1), DataInstance(i_2))          => i_1.partial_cmp(i_2),

            (CodataInstance(i_1), CodataInstance(i_2))      => i_1.partial_cmp(i_2),

            (PatternMatching(m_1), PatternMatching(m_2))    => m_1.partial_cmp(m_2),

            _                                               => None
        }
    }
}
