use crate::core::expr;
use crate::core::types;
use crate::core::context;

// Type parameter `R` is used in expressions which don't reduce to
// the same type (e.g. `Call` reducing to `Expr`).
//
pub trait Substitutable<'a> {
    fn rename(&mut self, orig: &str, new: String);

    fn substitute_val(&mut self, name: String, val: expr::Expr<'a>);
    //fn substitute_dim
}

pub trait Contextual<'a> {
    fn get_context(&self) -> &context::Context<'a>;

    fn get_context_mut(&mut self) -> &mut context::Context<'a>;
}

pub trait Typeable<'a> {
    fn get_type(&self) -> types::Type<'a>;
}

pub trait ReducibleInto<'a, T> {
    fn reduce(self, under: &context::Context<'a>) -> T;
}

pub trait Reducible<'a> {
    fn reduce(&mut self, under: &context::Context<'a>);
}
