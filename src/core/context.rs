use crate::core;
use crate::utils::declaration::Decl;
use crate::core::traits::Substitutable;

use std::collections::HashMap;


type ValDecl<'a> = Decl<core::types::Type<'a>, core::expr::Expr<'a>>;
type DimDecl<'a> = Decl<(), ()>;//FIXME

#[derive(Clone, Debug, PartialEq)]
pub struct Context<'a> {
    vals: HashMap<String, Box<ValDecl<'a>>>,
    dims: HashMap<String, DimDecl<'a>>,// FIXME
}

impl<'a> Context<'a> {
    pub fn empty() -> Self {
        Context {
            vals: HashMap::new(),
            dims: HashMap::new(),
        }
    }

    pub fn generate_fresh(&self, original: &str, at_least_once: bool) -> String {
        let mut new_name = String::from(original);

        let mut requires_iteration = at_least_once;

        while requires_iteration || self.binds_name(&new_name) {
            new_name.push('\'');

            requires_iteration = false;
        }

        new_name
    }

    pub fn rebind_val(&mut self, name: String, val: ValDecl<'a>) {
        self.vals.insert(name, Box::new(val));
    }

    pub fn rename(&mut self, orig: &str, new: String) {
        for v in self.vals.values_mut() {
            v.map_mut(
                &|ty| ty.rename(orig, new.clone()),
                &|val| val.rename(orig, new.clone()),
            );
        }

        // TODO
        //
        // for v in self.dims.values_mut() {
        //     *v = Box::new(
        //         v.clone().map(
        //             &|ty| ty.rename(orig, new.clone()),
        //             &|val| val.rename(orig, new.clone()),
        //         )
        //     );
        // }

        if let Some(orig_decl) = self.dims.remove_entry(orig) {
            self.dims.insert(new.clone(), orig_decl.1);
        }

        if let Some(orig_decl) = self.vals.remove_entry(orig) {
            self.vals.insert(new, orig_decl.1);
        }
    }

    pub fn get_val(&self, name: &str) -> Decl<core::types::Type<'a>, core::expr::Expr<'a>> {
        let v = self.vals.get(name).cloned().unwrap_or(Box::new(Decl::Nothing));

        *v
    }

    pub fn get_dim(&self, name: &str) -> DimDecl<'a> {
        self.dims.get(name).cloned().unwrap_or(Decl::Nothing)
    }

    pub fn binds_name(&self, name: &str) -> bool {
        self.vals.contains_key(name) || self.dims.contains_key(name)
    }

    pub fn generalizes(&self, other: &Self) -> bool {
        let mut acc = true;

        for (n, v) in &self.vals {
            let other_v = other.get_val(n);

            acc = acc && **v <= other_v;
        }

        for (n, v) in &self.dims {
            acc = acc && other.get_dim(n) == *v;//FIXME
        }

        acc
    }
}

impl<'a> PartialOrd for Context<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        let le = self.generalizes(other);
        let ge = other.generalizes(self);

        if le && ge {
            Some(std::cmp::Ordering::Equal)

        } else if le {
            Some(std::cmp::Ordering::Less)

        } else if ge {
            Some(std::cmp::Ordering::Greater)

        } else {
            None
        }
    }
}
