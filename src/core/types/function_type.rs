use crate::utils::err_msg;
use crate::utils::ty_tools;
use crate::utils::declaration::Decl;

use crate::core::traits::*;
use crate::core::expr::Expr;
use crate::core::types::Type;
use crate::core::context::Context;


#[derive(Clone, Debug, PartialEq)]
pub struct FunctionT<'a> {
    context: Context<'a>,

    elim: Option<String>,
    source: Box<Type<'a>>,
    target: Box<Type<'a>>,
}

impl<'a> FunctionT<'a> {
    pub fn new(elim: Option<String>, source: Type<'a>, target: Type<'a>) -> Self {
        let context = source.get_context().clone();
        let target_context = target.get_context();

        if let Some(ref name) = elim {
            assert![!context.binds_name(&name), err_msg::ctx_name_conflict(&name)];
            assert![target_context.binds_name(&name), err_msg::func_ctx_bind_elim(&name)];
        }

        assert![context.generalizes(&target_context), err_msg::FUNC_TY_CTX_CONFLICT];

        FunctionT {
            context,

            elim,

            source: Box::new(source),
            target: Box::new(target),
        }
    }

    pub fn get_source(&self) -> &Type<'a> {
        &self.source
    }

    pub fn get_source_mut(&mut self) -> &mut Type<'a> {
        &mut self.source
    }

    pub fn get_target(&self) -> (&Option<String>, &Type<'a>) {
        (&self.elim, &self.target)
    }

    pub fn get_target_mut(&mut self) -> (&mut Option<String>, &mut Type<'a>) {
        (&mut self.elim, &mut self.target)
    }

    pub fn get_context_mut(&mut self) -> &mut Context<'a> {
        &mut self.context
    }

    pub fn reduce_target(&mut self, under: &Context<'a>) {
        assert![self.context.generalizes(under), err_msg::REDUCE_CTX_CONFLICT];

        let mut inner_under = under.clone();

        if let Some(ref name) = self.elim {
            inner_under.rebind_val(name.clone(), Decl::Declared((*self.source).clone()));
        }

        (*self.target).reduce(&inner_under);
        self.context = under.clone();
    }

    pub fn reduce_source(&mut self, under: &Context<'a>) {
        assert![self.context.generalizes(under), err_msg::REDUCE_CTX_CONFLICT];

        (*self.source).reduce(&under);
        self.context = under.clone();
    }
}

impl<'a> Substitutable<'a> for FunctionT<'a> {
    fn rename(&mut self, orig: &str, new: String) {
        if orig != new {
            self.source.rename(orig.clone(), new.clone());
            self.target.rename(orig, new);
        }
    }

    fn substitute_val(&mut self, name: String, val: Expr<'a>) {
        ty_tools::check_subst(&self.get_context(), &name, &val);

        // NOTE: `self.context` binds `name` ->> it cannot clash with `self.elim`

        self.context.rebind_val(name.clone(), Decl::Defined(val.clone()));

        self.source.substitute_val(name.clone(), val.clone());
        self.target.substitute_val(name, val);
    }
}

impl<'a> Contextual<'a> for FunctionT<'a> {
    fn get_context(&self) -> &Context<'a> {
        &self.context
    }

    fn get_context_mut(&mut self) -> &mut Context<'a> {
        &mut self.context
    }
}

impl<'a> Typeable<'a> for FunctionT<'a> {
    fn get_type(&self) -> Type<'a> {
        Type::TypeT(self.get_context().clone())
    }
}

impl<'a> Reducible<'a> for FunctionT<'a> {
    fn reduce(&mut self, under: &Context<'a>) {
        assert![self.context.generalizes(under), err_msg::REDUCE_CTX_CONFLICT];

        self.source.reduce(under);

        let mut inner_under = under.clone();

        if let Some(ref name) = self.elim {
            inner_under.rebind_val(name.clone(), Decl::Declared((*self.source).clone()));
        }

        self.target.reduce(&inner_under);
        self.context = under.clone();
    }
}

impl<'a> PartialOrd for FunctionT<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        let mut ord = self.context.partial_cmp(&other.context);

        if let Some(elim) = &self.elim {
            if let Some(o_elim) = &other.elim {
                let new_elim = other.context.generate_fresh(elim, false);

                let mut r_self = self.clone();
                r_self.rename(elim, new_elim.clone());

                let mut r_other = other.clone();
                r_other.rename(o_elim, new_elim);

                ord = ty_tools::combine_partial_ord(ord, r_self.source.partial_cmp(&r_other.source));
                ord = ty_tools::combine_partial_ord(ord, r_self.target.partial_cmp(&r_other.target));

                ord

            } else {
                ord = ty_tools::combine_partial_ord(ord, self.source.partial_cmp(&other.source));
                ord = ty_tools::combine_partial_ord(ord, self.target.partial_cmp(&other.target));

                ord
            }

        } else {
            ord = ty_tools::combine_partial_ord(ord, self.source.partial_cmp(&other.source));
            ord = ty_tools::combine_partial_ord(ord, self.target.partial_cmp(&other.target));

            ord
        }
    }
}
