pub mod trees;
pub mod function_type;

use crate::utils::err_msg;
use crate::utils::ty_tools;
use crate::utils::declaration::Decl;

use crate::core::expr;
use crate::core::traits::*;
use crate::core::context::Context;

// TODO: Add Trail, Path, Morph
//
#[derive(Clone, Debug)]
pub enum Type<'a> {
    TypeT(Context<'a>),
    Zero(Context<'a>),

    Function(function_type::FunctionT<'a>),

    Data(*mut trees::data::Data<'a>),
    Codata(*mut trees::codata::Codata<'a>),
}

// NOTE: All the functions except `get_type` are inherently unsafe.
//
impl<'a> Substitutable<'a> for Type<'a> {
    fn rename(&mut self, orig: &str, new: String) {
        unsafe {
            use Type::*;
            match self {
                TypeT(c)    => c.rename(orig, new),
                Zero(c)     => c.rename(orig, new),

                Function(f) => f.rename(orig, new),

                Data(t)     => (**t).rename(orig, new),
                Codata(t)   => (**t).rename(orig, new),
            }
        }
    }

    fn substitute_val(&mut self, name: String, val: expr::Expr<'a>) {
        ty_tools::check_subst(&self.get_context(), &name, &val);

        unsafe {
            use Type::*;

            match self {
                TypeT(c) | Zero(c)  => {
                    ty_tools::check_subst(&c, &name, &val);

                    c.rebind_val(name, Decl::Defined(val));
                },

                Function(f)         => f.substitute_val(name, val),

                Data(t)             => (**t).substitute_val(name, val),
                Codata(t)           => (**t).substitute_val(name, val),
            }
        }
    }
}

impl<'a> Contextual<'a> for Type<'a> {
    fn get_context(&self) -> &Context<'a> {
        unsafe {
            use Type::*;
            match &self {
                TypeT(c) | Zero(c)  => c,

                Function(f)         => f.get_context(),
                Data(t)             => (**t).get_context(),
                Codata(t)           => (**t).get_context(),
            }
        }
    }

    fn get_context_mut(&mut self) -> &mut Context<'a> {
        unsafe {
            use Type::*;
            match self {
                TypeT(c) | Zero(c)  => c,

                Function(f)         => f.get_context_mut(),
                Data(t)             => (**t).get_context_mut(),
                Codata(t)           => (**t).get_context_mut(),
            }
        }
    }
}

impl<'a> Typeable<'a> for Type<'a> {
    fn get_type(&self) -> Self {
        Type::TypeT(self.get_context().clone())
    }
}

impl<'a> Reducible<'a> for Type<'a> {
    fn reduce(&mut self, under: &Context<'a>) {
        unsafe {
            use Type::*;
            match self {
                TypeT(c)    => { assert![c.generalizes(under), err_msg::REDUCE_CTX_CONFLICT]; *self = TypeT(under.clone()); },
                Zero(c)     => { assert![c.generalizes(under), err_msg::REDUCE_CTX_CONFLICT]; *self = Zero(under.clone()); },

                Function(f) => f.reduce(under),

                Data(t)     => (**t).reduce(under),
                Codata(t)   => (**t).reduce(under),
            }
        }
    }
}

impl<'a> PartialEq for Type<'a> {
    fn eq(&self, rhs: &Self) -> bool {
        use Type::*;
        match (self, rhs) {
            (TypeT(c), TypeT(d)) | (Zero(c), Zero(d))   => c == d,

            (Function(f_1), Function(f_2))              => f_1 == f_2,

            (Data(p_1), Data(p_2))                      => std::ptr::eq(*p_1, *p_2),
            (Codata(p_1), Codata(p_2))                  => std::ptr::eq(*p_1, *p_2),

            _                                           => false,
        }
    }
}

impl<'a> PartialOrd for Type<'a> {
    fn partial_cmp(&self, rhs: &Self) -> Option<std::cmp::Ordering> {
        use Type::*;
        match (self, rhs) {
            (TypeT(c), TypeT(d)) | (Zero(c), Zero(d)) => c.partial_cmp(d),

            (Function(f_1), Function(f_2)) => f_1.partial_cmp(f_2),

            (Data(p_1), Data(p_2)) => {
                if std::ptr::eq(*p_1, *p_2) {
                    Some(std::cmp::Ordering::Equal)

                } else {
                    None
                }
            },

            (Codata(p_1), Codata(p_2)) => {
                if std::ptr::eq(*p_1, *p_2) {
                    Some(std::cmp::Ordering::Equal)

                } else {
                    None
                }
            },

            _ => None,
        }
    }
}
