use crate::utils::err_msg;
use crate::utils::ty_tools;
use crate::utils::declaration::Decl;

use crate::core::traits::*;
use crate::core::expr::Expr;
use crate::core::types::Type;
use crate::core::context::Context;
use crate::core::types::function_type::FunctionT;

use std::collections::HashMap;


#[derive(Clone, Debug, PartialEq)]
pub struct Codata<'a> {
    items: HashMap<&'a str, FunctionT<'a>>,

    context: Context<'a>,
}

impl<'a> Codata<'a> {
    pub fn init(context: Context<'a>) -> Self {
        Codata {
            items: HashMap::new(),

            context,
        }
    }

    pub fn add_item(&mut self, name: &'a str, f: FunctionT<'a>) {
        assert![!self.context.binds_name(name), err_msg::ctx_name_conflict(name)];
        assert![!self.items.contains_key(name), err_msg::tree_name_conflict(name)];

        assert![self.context.generalizes(&f.get_context()), err_msg::d_tree_elim_ctx_conflict(name)];

        if let Type::Codata(p_tree) = f.get_source() {
            let eq = std::ptr::eq(*p_tree, self as *const Codata<'a>);

            assert![eq, err_msg::d_tree_elim_ty_invalid(name)];

        } else {
            panic![err_msg::d_tree_elim_ty_invalid(name)]
        }

        // NOTE: Storing a `FunctionT` since it's easier to work with.
        //
        self.items.insert(name, f);
    }

    pub fn get_item(&self, name: &str) -> Option<&FunctionT<'a>> {
        self.items.get(name)
    }
}

// NOTE: Only substituting into target; the source is identical to `self`.
//
impl<'a> Substitutable<'a> for Codata<'a> {
    fn rename(&mut self, orig: &str, new: String) {
        for i in self.items.values_mut() {
            i.get_target_mut().1.rename(orig, new.clone());
        }

        self.context.rename(orig, new);
    }

    fn substitute_val(&mut self, name: String, val: Expr<'a>) {
        ty_tools::check_subst(&self.get_context(), &name, &val);

        for i in self.items.values_mut() {
            i.get_target_mut().1.substitute_val(name.clone(), val.clone())
        }

        self.context.rebind_val(name, Decl::Defined(val));
    }
}

impl<'a> Contextual<'a> for Codata<'a> {
    fn get_context(&self) -> &Context<'a> {
        &self.context
    }

    fn get_context_mut(&mut self) -> &mut Context<'a> {
        &mut self.context
    }
}

impl<'a> Typeable<'a> for Codata<'a> {
    fn get_type(&self) -> Type<'a> {
        Type::TypeT(self.get_context().clone())
    }
}

// NOTE: Only reducing target; the source is identical to `self`.
//
impl<'a> Reducible<'a> for Codata<'a> {
    fn reduce(&mut self, under: &Context<'a>) {
        for (k, v) in self.items.iter_mut() {
            assert![!under.binds_name(k), err_msg::reduce_ctx_name_conflict(k)];
            assert![v.get_context().generalizes(under), err_msg::REDUCE_CTX_CONFLICT];

            (*v).reduce_target(under);
        }

        self.context = under.clone();
    }
}
