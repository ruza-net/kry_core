use crate::utils::err_msg;
use crate::utils::ty_tools;
use crate::utils::mut_tools::Lock;
use crate::utils::declaration::Decl;

use crate::core::traits::*;
use crate::core::expr::Expr;
use crate::core::types::Type;
use crate::core::context::Context;
use crate::core::types::function_type::FunctionT;

use std::collections::HashMap;


type Item<'a> = FunctionT<'a>;


#[derive(Clone, Debug, PartialEq)]
pub struct Data<'a> {
    items: HashMap<&'a str, Item<'a>>,

    mut_lock: Lock,

    context: Context<'a>,
}

impl<'a> Data<'a> {
    pub fn init(context: Context<'a>) -> Self {
        Data {
            items: HashMap::new(),
            mut_lock: Lock::off(),

            context,
        }
    }

    // FIXME: Type and constructor dependency.
    //
    pub fn add_item(&mut self, name: &'a str, f: Item<'a>) {
        assert![!self.context.binds_name(name), err_msg::ctx_name_conflict(name)];
        assert![!self.items.contains_key(name), err_msg::tree_name_conflict(name)];

        assert![self.context.generalizes(&f.get_context()), err_msg::c_tree_cons_ctx_conflict(name)];

        // FIXME: Here
        //
        if let (_elim, Type::Data(p_tree)) = f.get_target() {
            let eq = std::ptr::eq(*p_tree, self as *const Data<'a>);

            assert![eq, err_msg::c_tree_cons_ty_invalid(name)];

        } else {
            panic![err_msg::c_tree_cons_ty_invalid(name)]
        }

        self.items.insert(name, f);
    }

    pub fn get_item(&self, name: &str) -> Option<&Item<'a>> {
        self.items.get(name)
    }

    pub fn get_items(&self) -> &HashMap<&'a str, Item<'a>> {
        &self.items
    }
}

// NOTE: Only substituting into source; the target is identical to `self`.
//
impl<'a> Substitutable<'a> for Data<'a> {
    fn rename(&mut self, orig: &str, new: String) {
        if orig != new {
            if !self.mut_lock.rename {
                self.mut_lock.rename = true;

                for i in self.items.values_mut() {
                    i.get_source_mut().rename(orig, new.clone());
                }

                self.mut_lock.rename = false;

                self.context.rename(orig, new);
            }
        }
    }

    fn substitute_val(&mut self, name: String, val: Expr<'a>) {
        ty_tools::check_subst(&self.get_context(), &name, &val);

        for i in self.items.values_mut() {
            i.get_source_mut().substitute_val(name.clone(), val.clone());
            i.get_context_mut().rebind_val(name.clone(), Decl::Defined(val.clone()));
        }

        self.context.rebind_val(name, Decl::Defined(val));
    }
}

impl<'a> Contextual<'a> for Data<'a> {
    fn get_context(&self) -> &Context<'a> {
        &self.context
    }

    fn get_context_mut(&mut self) -> &mut Context<'a> {
        &mut self.context
    }
}

impl<'a> Typeable<'a> for Data<'a> {
    fn get_type(&self) -> Type<'a> {
        Type::TypeT(self.get_context().clone())
    }
}

// NOTE: Only reducing source; the target is identical to `self`.
//
impl<'a> Reducible<'a> for Data<'a> {
    fn reduce(&mut self, under: &Context<'a>) {
        for (k, v) in self.items.iter_mut() {
            assert![!under.binds_name(k), err_msg::reduce_ctx_name_conflict(k)];
            assert![v.get_context().generalizes(under), err_msg::REDUCE_CTX_CONFLICT];

            (*v).reduce_source(under);
        }

        self.context = under.clone();
    }
}
