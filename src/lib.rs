pub mod core;
pub mod utils;


#[cfg(test)]
mod tests {
    use crate::core::expr;
    use crate::core::types;
    use crate::utils;

    use crate::core::context::Context;
    use crate::core::traits::*;

    use utils::common_stuff;
    use utils::declaration::Decl;

    use types::trees::data::Data;
    use types::trees::codata::Codata;
    use types::function_type::FunctionT;

    use expr::trees::data_instance::{ DataInstance, Pattern, PatternMatching };

    fn _make_foo_data<'a>(
        out_data: &mut Data<'a>,
        unit: &mut DataInstance<'a>,
    ) -> DataInstance<'a> {
        let f_ty = FunctionT::new(
            None,
            unit.get_type(),
            types::Type::Data(out_data as *mut Data),
        );

        (*out_data).add_item("foo", f_ty);

        DataInstance::new(
            out_data as *mut Data,
            "foo",
            expr::Expr::DataInstance(unit as *mut DataInstance),
        )
    }

    #[test]
    fn reduce_context() {
        let c = Context::empty();

        let mut ty = types::Type::TypeT(c);

        let mut under = Context::empty();
        under.rebind_val("foo".to_string(), Decl::Nothing);

        ty.reduce(&under);

        assert_eq![*ty.get_context(), under, "context didn't get updated"];
    }

    #[test]
    fn data_initialisation() {
        let mut unit_ty = Data::init(Context::empty());
        let mut unit = common_stuff::make_unit(&mut unit_ty, "()", Context::empty());

        let mut foo_data = Data::init(Context::empty());

        _make_foo_data(&mut foo_data, &mut unit);
    }

    #[test]
    fn patterns() {
        let mut unit_ty = Data::init(Context::empty());
        let mut unit = common_stuff::make_unit(&mut unit_ty, "()", Context::empty());

        let mut foo_data = Data::init(Context::empty());
        let mut foo_instance = _make_foo_data(&mut foo_data, &mut unit);

        use Pattern::*;
        let pat = Constructor("foo", Box::new(Capture("u")));

        let (name, val) = pat
            .destruct(expr::Expr::DataInstance(&mut foo_instance as *mut _))
            .expect("couldn't destruct");

        assert_eq!["u", name, "capture names don't match"];
        assert_eq![
            expr::Expr::DataInstance(&mut unit as *mut _),
            val,
            "captured values don't match"
        ];
    }

    #[test]
    fn pattern_matching() {
        let mut bool_data = Data::init(Context::empty());
        let (mut v_true, mut v_false) =
            common_stuff::make_bool(&mut bool_data, "true", "false", Context::empty());

        let e_true = expr::Expr::DataInstance(&mut v_true as *mut DataInstance);
        let e_false = expr::Expr::DataInstance(&mut v_false as *mut DataInstance);

        let bool_ty = types::Type::Data(&mut bool_data as *mut Data);

        let f_ty = FunctionT::new(None, bool_ty.clone(), bool_ty.clone());

        let mut inner_context = Context::empty();
        inner_context.rebind_val("b".to_owned(), Decl::Declared(bool_ty.clone()));

        let mut pat_match = PatternMatching::new(
            expr::Expr::Atom("b".to_owned(), inner_context.clone()),
            bool_ty.clone(),
        );

        let true_pat = Pattern::Constructor("true", Box::new(Pattern::Capture("_")));
        let false_pat = Pattern::Constructor("false", Box::new(Pattern::Capture("_")));

        let mut inner_true = e_true.clone();
        let mut inner_false = e_false.clone();

        // NOTE: To update their contexts.
        //
        inner_true.reduce(&inner_context);
        inner_false.reduce(&inner_context);

        pat_match.add_pat(true_pat, inner_false);
        pat_match.add_pat(false_pat, inner_true);

        let f = expr::functions::function::Function::new("b".to_owned(), bool_ty, expr::Expr::PatternMatching(pat_match), f_ty);

        let ret_1 = f.clone().call(e_true.clone());
        let ret_2 = f.call(e_false.clone());

        assert_eq![e_false, ret_1];
        assert_eq![e_true, ret_2];
    }

    #[test]
    fn codata_instantiation() {
        let mut d_tree = Codata::init(Context::empty());

        let tree_ty = types::Type::Codata(&mut d_tree as *mut Codata);

        let f = FunctionT::new(None, tree_ty, types::Type::Zero(Context::empty()));

        d_tree.add_item("foo", f);

        {
            if let types::Type::Codata(t) = d_tree.get_item("foo").unwrap().get_source() {
                let eq = std::ptr::eq(*t, &d_tree as *const Codata);

                assert![eq, "eliminator source not original tree"];
            } else {
                panic!["eliminator source is not Type::Codata"];
            }
        }
    }
}
