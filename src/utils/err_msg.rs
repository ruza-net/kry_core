use std::fmt::Debug;
use crate::core::expr::trees::data_instance::Pattern;


// [[ Internal Bug ]]
//
pub fn      internal_tree_item_defined(name: &str) -> String {
    format!["INTERNAL: The {} of a divergent tree is Decl::Defined.", name]
}

// [[ General ]]
//
pub const   REDUCE_CTX_CONFLICT: &str = "The reducing context is not compatible with the expression's context.";

pub const   FUNC_TY_CTX_CONFLICT: &str = "The context of the target type is not compatible with the outer context.";

pub const   CALL_TY_CONFLICT: &str = "Only function expressions are callable.";

pub fn      func_arg_name_conflict(fn_name: &str, ty_name: &str) -> String {
    format!["Function argument name doesn't match with the type.\n\tFunction: {}\n\tType: {}", fn_name, ty_name]
}

pub fn      func_arg_ty_conflict(name: &str) -> String {
    format!["Invalid type passed to function {}.", name]
}

pub fn      func_ret_ty_conflict(name: &str) -> String {
    format!["Invalid type returned from function {}.", name]
}

pub fn      func_ctx_bind_elim(name: &str) -> String {
    format!["The context of the target does not bind {}.", name]
}

pub fn      func_ctx_conflict(name: &str) -> String {
    format!["Function body's context is not an extension of external context by {}.", name]
}

pub fn      ctx_name_conflict(name: &str) -> String {
    format!["The context already binds {}.", name]
}

pub fn      reduce_ctx_name_conflict(name: &str) -> String {
    format!["The reducing context binds the external {}.", name]
}

pub fn      ctx_name_not_declared(name: &str) -> String {
    format!["Current context does not declare {}.", name]
}


// [[ Substitutions ]]
//
pub fn      subst_type_conflict(name: &str) -> String {
    format!["The declared type of {} does not match the substituting value.", name]
}

pub fn      ctx_subst_def_conflict(name: &str) -> String {
    format!["Cannot substitute for {}, which is already defined.", name]
}


// [[ General Trees ]]
//
pub fn      tree_name_conflict(name: &str) -> String {
    format!["The item {} is already declared.", name]
}


// [[ Divergent Trees ]]
//
pub fn      d_tree_elim_ctx_conflict(name: &str) -> String {
    format!["The context of eliminator {} is not compatible with the type's context.", name]
}

pub fn      d_tree_elim_ty_invalid(name: &str) -> String {
    format!["The eliminator {} is not a function from the type.", name]
}

pub fn      d_tree_item_not_decl(name: &str) -> String {
    format!["The divergent tree has no eliminator {}.", name]
}

pub fn      d_tree_def_conflict(name: &str) -> String {
    format!["Conflicting definitions of eliminator {}.", name]
}


// [[ Convergent Trees ]]
//
pub fn      c_tree_cons_ctx_conflict(name: &str) -> String {
    format!["The context of constructor {} is not compatible with the type's context.", name]
}

pub fn      c_tree_cons_ty_invalid(name: &str) -> String {
    format!["The constructor {} is not a function into the type.", name]
}

pub fn      c_tree_item_not_decl(name: &str) -> String {
    format!["The convergent tree has no constructor {}.", name]
}

pub fn      c_tree_cons_pat_invalid<E: Debug>(pat: &Pattern<E>) -> String {
    format!["Invalid pattern for given data type: {:?}", pat]
}


// [[ Patterns ]]
//
pub const   PATS_NOT_REFUTABLE: &str = "Pattern matching doesn't specify all cases.";

pub const   MATCH_NOT_ON_DATA: &str = "Pattern matching on non-data type.";

pub fn      pat_type_inconsistent<E: Debug>(pat: &Pattern<E>) -> String {
    format!["Invalid type of expression associated with pattern {:?}", pat]
}

pub fn      pat_exists<E: Debug>(pat: &Pattern<E>) -> String {
    format!["Pattern already specified: {:?}", pat]
}

pub fn      pat_captures_conflict<E: Debug>(pat: &Pattern<E>) -> String {
    format!["Subpattern has conflicts with other captures in the whole pattern: {:?}", pat]
}

pub fn      match_invalid_cons(name: &str) -> String {
    format!["Pattern matching with invalid constructor {}", name]
}
