use std::fmt::Debug;
use std::cmp::{ PartialOrd, Ordering };

use crate::core::types::Type;
use crate::core::traits::Typeable;


#[derive(Debug, Clone, PartialEq)]
pub enum Decl<T: Debug, E: Debug> {
    Nothing,

    Declared(T),
    Defined(E),
}

impl<T: Debug, E: Debug> Decl<T, E> {
    pub fn map<T2: Debug, E2: Debug>(self, f: &dyn Fn(T) -> T2, g: &dyn Fn(E) -> E2) -> Decl<T2, E2> {
        use Decl::*;
        match self {
            Nothing     => Nothing,

            Declared(t) => Declared(f(t)),
            Defined(e)  => Defined(g(e)),
        }
    }

    pub fn map_mut<T2: Debug, E2: Debug>(&mut self, f: &dyn Fn(&mut T) -> T2, g: &dyn Fn(&mut E) -> E2) -> Decl<T2, E2> {
        use Decl::*;
        match self {
            Nothing     => Nothing,
            Declared(t) => Declared(f(t)),
            Defined(e)  => Defined(g(e)),
        }
    }

    pub fn unwrap_decl(self) -> T {
        match self {
            Decl::Declared(d) => d,

            dc => panic!["unwrap_decl on {:?}", dc],
        }
    }

    pub fn unwrap_def_or(self, ret: E) -> E {
        if let Decl::Defined(e) = self {
            e

        } else {
            ret
        }
    }

    pub fn is_nothing(&self) -> bool {
        match self {
            Decl::Nothing => true,

            _ => false,
        }
    }
}

impl<'a, E: PartialOrd + Debug + Typeable<'a>> PartialOrd for Decl<Type<'a>, E> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        use Decl::*;

        match (self, other) {
            (Declared(ty_1), Declared(ty_2)) => ty_1.partial_cmp(ty_2),

            (Defined(val_1), Defined(val_2)) => val_1.partial_cmp(val_2),

            (Defined(val), Declared(ty)) => {
                if *ty <= val.get_type() {
                    Some(Ordering::Greater)

                } else {
                    None
                }
            },

            (Declared(ty), Defined(val)) => {
                if *ty <= val.get_type() {
                    Some(Ordering::Less)

                } else {
                    None
                }
            },

            _ => None
        }
    }
}
