use crate::utils::err_msg;
use crate::utils::declaration::Decl;

use crate::core::expr;
use crate::core::traits::*;
use crate::core::context::Context;


pub fn check_subst<'a>(c: &Context<'a>, name: &str, val: &expr::Expr<'a>) {
    match c.get_val(name) {
        Decl::Nothing => panic![err_msg::ctx_name_not_declared(name)],

        Decl::Defined(old_val) => assert![old_val <= *val, err_msg::ctx_subst_def_conflict(name)],

        Decl::Declared(t) => assert![t <= val.get_type(), err_msg::subst_type_conflict(name)],
    };
}

pub fn combine_partial_ord(o_1: Option<std::cmp::Ordering>, o_2: Option<std::cmp::Ordering>) -> Option<std::cmp::Ordering> {
    use std::cmp::Ordering::*;

    match (o_1, o_2) {
        (None, _) => None,
        (_, None) => None,

        (Some(Less), Some(o)) | (Some(o), Some(Less)) if o != Greater => Some(Less),

        (Some(Greater), Some(o)) | (Some(o), Some(Greater)) if o != Less => Some(Greater),

        (Some(Equal), Some(Equal)) => Some(Equal),

        _ => None,
    }
}
