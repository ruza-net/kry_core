#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Lock {
    pub rename: bool,
    pub reduce: bool,
    pub substitute_val: bool,
}

impl Lock {
    pub fn off() -> Self {
        Lock {
            rename: false,
            reduce: false,
            substitute_val: false,
        }
    }
}
