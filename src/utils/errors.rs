use crate::core::expr;
use expr::trees::data_instance::Pattern;

#[derive(Debug)]
pub enum CopatternError<'a> {
    NoSuchEliminator(&'a str),
    EliminatorNotSpecified(&'a str),
}

#[derive(Debug)]
pub enum PatternError<'a, E> {
    NotMatching(Pattern<'a, E>, expr::Expr<'a>),
}
