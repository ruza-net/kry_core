use crate::core::expr;
use crate::core::types;
use crate::core::context::Context;

use crate::utils::declaration::Decl;

use types::trees::data::Data;
use types::function_type::FunctionT;

use expr::functions::function::Function;
use expr::trees::data_instance as data;


pub fn make_zero_map<'a>(context: Context<'a>) -> (FunctionT, expr::Expr<'a>) {
    let zero = types::Type::Zero(context.clone());
    let zero_map_ty = FunctionT::new(None, zero.clone(), zero.clone());

    let mut inner_context = context;
    inner_context.rebind_val("z".to_owned(), Decl::Declared(zero.clone()));

    (zero_map_ty.clone(), expr::Expr::Function(Function::new("z".to_string(), zero, expr::Expr::Atom("z".to_string(), inner_context), zero_map_ty)))
}

pub fn make_unit<'a>(unit_data: &mut Data<'a>, unit_name: &'a str, context: Context<'a>) -> data::DataInstance<'a> {
    let (zero_map_ty, zero_map) = make_zero_map(context);

    let unit_cons_ty = FunctionT::new(None, types::Type::Function(zero_map_ty.clone()), types::Type::Data(unit_data as *mut Data));

    (*unit_data).add_item(unit_name, unit_cons_ty.clone());

    data::DataInstance::new(unit_data as *mut Data, unit_name, zero_map)
}

pub fn make_bool<'a>(bool_data: &mut Data<'a>, true_name: &'a str, false_name: &'a str, context: Context<'a>) -> (data::DataInstance<'a>, data::DataInstance<'a>) {
    let (zero_map_ty, zero_map) = make_zero_map(context);

    let cons_ty = FunctionT::new(None, types::Type::Function(zero_map_ty), types::Type::Data(bool_data as *mut Data));

    (*bool_data).add_item(true_name, cons_ty.clone());
    (*bool_data).add_item(false_name, cons_ty);

    (
        data::DataInstance::new(bool_data as *mut Data, true_name, zero_map.clone()),
        data::DataInstance::new(bool_data as *mut Data, false_name, zero_map),
    )
}
