# kry_core

Typechecker kernel for a functional programming language with dependent types, data, codata and cubical propositional equality.


## Notes on Design Decisions

### Data Representation

I didn't want to make the typechecker nominal, so I needed some of the internal datatypes to store references to themselves.
Since Rust prohibits this in order to guarantee memory safety, I had to use raw pointers extensively. Their most prominent usage
is in the `Type` enum, which holds raw pointers to the specific structs (`DivergentTree`, `ConvergentTree` and `Function`).
A `DTreeInstance` also holds a raw pointer to its corresponding `DivergentTree`.

These raw pointers force some of the `Contextual::...` implementations to contain `unsafe` blocks.

### Mutability

Since some of the structures only hold raw pointers to other structures, I had to make all the effectful methods of `Contextual`
as methods mutating the instance. I couldn't make them return a modified instance, since I would then have to produce new raw pointers
to the modified data. But these would presumably get dropped, invalidating the memory, so I had to modify them in-place.